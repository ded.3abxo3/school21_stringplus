#include "s21_string.h"

/* Выполняет поиск первого вхождения символа c (беззнаковый тип) в первых n
байтах строки, на которую указывает аргумент str.

Детальный разбор функции. Последующие функции сделаны по подобию этой */

void *s21_memchr(const void *str, int c, s21_size_t n) {
  /* преобразование динамического типа в unsigned char (8бит),
   указатель s указывает на str */
  s21_uchar *s = (s21_uchar *)str;

  /* устанавливаем результат равным NULL */
  void *result = NULL;

  /* пробегаемся по строке от 0 до n */
  for (s21_size_t i = 0; i < n; i++) {
    /* если находим символ, то результату => */
    if (s[i] == (s21_uchar)c) {
      result = (void *)(s + i);  // => присваиваем указатель на место нахождения
                                 // символа в памяти
      i = n;                     // выход из цикла
    }
  }
  return result;  // возврат указателя
}

/* Сравнивает первые n байтов str1 и str2 */
int s21_memcmp(const void *str1, const void *str2, s21_size_t n) {
  const s21_uchar *p1 = str1, *p2 = str2;
  int difference = 0;
  while (n-- > 0) {
    if (*p1 != *p2) {
      difference = *p1 - *p2;
      break;
    } else {
      p1++;
      p2++;
    }
  }
  return difference;
}

/* Копирует n символов из src в dest */
void *s21_memcpy(void *dest, const void *src, s21_size_t n) {
  s21_uchar *destination = (s21_uchar *)dest;
  const s21_uchar *source = (const s21_uchar *)src;
  for (s21_size_t j = 0; j < n; j++) {
    destination[j] = source[j];
  }
  return dest;
}

/* Еще одна функция для копирования n символов из str2 в str1 */
void *s21_memmove(void *dest, const void *src, s21_size_t n) {
  s21_uchar *destination = (s21_uchar *)dest;
  s21_uchar *source = (s21_uchar *)src;

  if (destination - source >= (long int)n) {
    dest = s21_memcpy(dest, src, n);
  } else {
    destination += n - 1;
    source += n - 1;
    while (n--) *destination-- = *source--;
  }
  return dest;
}

/* Копирует символ sym (беззнаковый тип) в первые n символов строки,
на которую указывает аргумент str */
void *s21_memset(void *str, int c, s21_size_t n) {
  s21_uchar *pstr = (s21_uchar *)str;
  for (s21_size_t i = 0; i < n; i++) {
    pstr[i] = (s21_uchar)c;
  }
  return pstr;
}
