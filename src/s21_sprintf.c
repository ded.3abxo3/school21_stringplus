#include "s21_string.h"

int s21_sprintf(char *str, const char *format, ...) {
  int result = 0;
  str[0] = '\0';
  va_list params;
  va_start(params, format);
  if (format != S21_NULL && str != S21_NULL) {
    result = process_string(str, format, params);
  } else {
    result = 0;
  }
  va_end(params);
  return result;
}

// инициализация структуры
Tmarker marker_init() {
  Tmarker init = {{0}, ' ', ' ', -1, -1, -1, FALSE};
  return init;
}

// разбираем строку с параметрами на параметры и просто символы
int process_string(char *str, const char *format, va_list params) {
  // printf("STRLEN: %d\n", (int)s21_strlen(str));
  int successCount = 0;
  Tmarker marker;
  char *startStr = str;  // указатель на начало строки
  char *runStr = str;  // указатель на позицию в строке (процессинг)
  while (*format) {
    marker = marker_init();
    if (*format != '%') {
      *runStr = *format;
      runStr++;
      *runStr = '\0';
      format++;
    } else {
      char buffer[BUF_SIZE] = "";
      format++;  // переключаем указатель на следующий символ после %
      int status = format_parsing(&format, &marker, params);
      if (status == -1) {
        *runStr = '%';
        runStr++;
        *runStr = '\0';
        format++;
        continue;
      }
      if (status) status = process_key(&marker, buffer, params);
      if (status) {
        // printf("BUF [%s] => ",buffer);
        s21_strcat(startStr, buffer);
        runStr += s21_strlen(buffer);
        *runStr = '\0';
        successCount++;
      } else {
        break;
      }
    }
    // printf("%s\n", startStr);
  }
  // printf("-------------------------\n");
  *runStr = '\0';
  return s21_strlen(startStr);
}

int format_parsing(const char **format, Tmarker *marker, va_list params) {
  int result = 1;

  // пробегаемся посимвольно и вычисляем параметры
  while (*format) {
    if (issign_macro(**format)) {
      set_sign(**format, marker);
      (*format)++;
    } else if (isdec_macro(**format) || (**format) == '*') {
      set_width_acc(format, params, 'w', marker);
    } else if (**format == '.') {
      marker->ifdot = TRUE;
      (*format)++;
      if (isdec_macro(**format) || (**format) == '*') {
        set_width_acc(format, params, 'a', marker);
      } else {
        marker->accuracy = 0;
        continue;
      }
    } else if (islen_macro(**format)) {
      marker->len = **format;
      (*format)++;
    } else if (**format == '%') {
      result = -1;
      break;
    } else if (isformat_macro(**format)) {
      marker->key = **format;
      (*format)++;
      result = 1;
      break;
    } else {
      result = 0;
      break;
    }
  }
  return result;
}

void set_sign(char sym, Tmarker *marker) {
  switch (sym) {
    case '+':
      (*marker).sign.ifplus = TRUE;
      break;
    case '-':
      (*marker).sign.ifminus = TRUE;
      break;
    case ' ':
      (*marker).sign.ifspace = TRUE;
      break;
    case '0':
      (*marker).sign.ifzero = TRUE;
      break;
    case '#':
      (*marker).sign.ifsharp = TRUE;
      break;
  }
}

int set_width_acc(const char **format, va_list params, char mode,
                  Tmarker *marker) {
  int status = 1;
  int *field = S21_NULL;
  if (mode == 'w') field = &(marker->width);
  if (mode == 'a') field = &(marker->accuracy);

  if (isdec_macro(**format)) {
    char buffer[BUF_SIZE] = "";
    find_digits(buffer, *format);
    *field = s21_atoi(buffer);
    *format += s21_strlen(buffer);
  } else if (**format == '*') {
    int value = va_arg(params, int);
    if (mode == 'w' && value < 0) {
      value *= -1;
      marker->sign.ifminus = TRUE;
    }
    *field = value;
    (*format)++;
  } else {
    status = 0;
  }
  return status;
}

char *find_digits(char *dest, const char *src) {
  char *result;
  bool ifdigit = FALSE;
  int i;
  for (i = 0; isdec_macro(src[i]); i++) {
    dest[i] = src[i];
    ifdigit = TRUE;
  }
  if (ifdigit) {
    dest[i] = '\0';
    result = dest;
  } else {
    result = S21_NULL;
  }
  return result;
}

int process_key(Tmarker *marker, char *buffer, va_list params) {
  int status = 1;
  if (marker->key == 'c') {
    status = process_char(marker, buffer, params);
  } else if (isint_macro(marker->key)) {
    status = process_int(marker, buffer, params);
  } else if (marker->key == 's') {
    status = process_str(marker, buffer, params);
  } else if (marker->key == 'f') {
    status = process_float(marker, buffer, params);
  } else if (marker->key == 'o') {
    status = process_octal(marker, buffer, params);
  } else if (marker->key == 'x' || marker->key == 'X') {
    status = process_hex(marker, buffer, params);
  } else if (marker->key == 'p') {
    status = process_pointer(/*marker,*/ buffer, params);
  } else if (marker->key == 'e' || marker->key == 'E') {
    status = process_eE(marker, buffer, params);
  } else if (marker->key == 'g' || marker->key == 'G') {
    status = process_eE(marker, buffer, params);
  } else {
    status = 0;
  }
  return status;
}

int process_char(Tmarker *marker, char *buffer, va_list params) {
  int status = 1;
  if (marker->width == -1) {
    marker->width = 1;
  }
  // printf("SPACE: %d\n", marker->sign.ifspace);
  int width = marker->width;
  char sym = (char)va_arg(params, int);
  // sym = abs(sym); // устранение отрицательных входных данных (char 0-254)
  int len = width;
  marker->fact_width = len;
  int i = 0;
  if (marker->sign.ifminus) {
    buffer[i] = sym;
    i++;
    for (; len > 1; len--) {
      buffer[i] = ' ';
      i++;
    }
  } else {
    for (; len > 1; len--) {
      buffer[i] = ' ';
      i++;
    }
    buffer[i] = sym;
  }
  return status;
}

int process_int(Tmarker *marker, char *buffer, va_list params) {
  int status = 1;
  unsigned long l_value;
  int i_value;
  unsigned ui_value;
  short sh_value;
  unsigned short ush_value;
  char tmpBuffer[BUF_SIZE] = "";  // обработка цифр в строку
  char length = marker->len;
  char key = marker->key;

  l_value = va_arg(params, unsigned long);
  if (length == 'l') {
    if (key == 'd' || key == 'i') {
      s21_itoa_long(marker, l_value, tmpBuffer);
    } else if (key == 'u') {
      unsigned long ul_value;
      ul_value = (unsigned long)l_value;
      s21_itoa_long(marker, ul_value, tmpBuffer);
    }
  } else if (length == 'h') {
    if (key == 'd' || key == 'i') {
      sh_value = (short)l_value;
      s21_itoa_long(marker, sh_value, tmpBuffer);
    } else if (key == 'u') {
      ush_value = (unsigned short)l_value;
      s21_itoa_long(marker, ush_value, tmpBuffer);
    }
  } else if (length == ' ') {
    if (key == 'd' || key == 'i') {
      i_value = (int)l_value;
      s21_itoa_long(marker, i_value, tmpBuffer);
    } else if (key == 'u') {
      ui_value = (unsigned int)l_value;
      s21_itoa_long(marker, ui_value, tmpBuffer);
    }
  }

  int sign = tmpBuffer[0] == '-' ? -1 : 1;
  int digits_len = sign > 0 ? s21_strlen(tmpBuffer) : s21_strlen(tmpBuffer) - 1;
  char lstBuf[BUF_SIZE] = "";
  char *lb_ptr = lstBuf;

  if (marker->sign.ifplus) {
    if (sign < 0) {
      lstBuf[0] = '-';
      lb_ptr++;
    } else {
      lstBuf[0] = '+';
      lb_ptr++;
    }
  } else {
    if (sign < 0) {
      lstBuf[0] = '-';
      lb_ptr++;
    } else if (marker->sign.ifspace && marker->key != 'u') {
      *lb_ptr = ' ';
      lb_ptr++;
    }
  }
  if (marker->accuracy != -1) {
    for (int i = marker->accuracy - digits_len; i > 0; i--) {
      *lb_ptr = '0';
      lb_ptr++;
    }
  }
  if ((s21_strcmp(tmpBuffer, "0") == 0) &&
      (marker->key == 'i' || marker->key == 'd')) {
    if (marker->accuracy < 0) {
      *lb_ptr = '0';
      // lb_ptr++;
    }
    marker->fact_width = 0;
  } else {
    if (marker->sign.ifzero == TRUE && marker->width >= digits_len) {
      int zeros = marker->width - digits_len;
      // if (sign < 0 || marker->sign.ifplus) zeros--;
      fillzeros(lb_ptr, zeros);
      lb_ptr += zeros;
    }
    if (sign < 0) {
      s21_strncpy(lb_ptr, tmpBuffer + 1, digits_len);
    } else {
      s21_strncpy(lb_ptr, tmpBuffer, digits_len);
    }
  }
  int fct_l_wrt = marker->width > (int)s21_strlen(lstBuf)
                      ? marker->width
                      : (int)s21_strlen(lstBuf);
  marker->fact_width = s21_strlen(lstBuf);
  if (marker->sign.ifminus) {
    s21_strcpy(buffer, lstBuf);
    for (int i = s21_strlen(lstBuf); i < fct_l_wrt; i++) {
      buffer[i] = ' ';
      marker->fact_width++;
    }
  } else {
    int i = 0;
    for (; i < fct_l_wrt - (int)s21_strlen(lstBuf); i++) {
      buffer[i] = ' ';
      marker->fact_width++;
    }
    s21_strcpy(buffer + i, lstBuf);
  }
  return status;
}

int process_str(Tmarker *marker, char *buffer, va_list params) {
  int result = 1;
  char *pinStr = (char *)va_arg(params, char *);
  char inStr[BUF_SIZE] = "";
  if (pinStr == S21_NULL)
    s21_strcat(inStr, "(null)");
  else
    s21_strcat(inStr, pinStr);
  // print_marker(*marker);
  int accuracy = marker->accuracy;
  int width = marker->width;
  int realLength = s21_strlen(inStr);
  int len_to_write = realLength;
  if (accuracy != -1) {
    len_to_write = accuracy < realLength ? accuracy : realLength;
  }
  if (marker->ifdot && marker->accuracy <= 0) {
    if (marker->width > 0) {
      for (int i = 0; i < marker->width; i++) buffer[i] = ' ';
    }
    result = 1;
  } else {
    marker->fact_width = len_to_write;
    if (width > len_to_write) {
      marker->fact_width = width;
      if (marker->sign.ifminus) {
        s21_strncpy(buffer, inStr, len_to_write);
        for (int i = len_to_write; i < width; i++) {
          buffer[i] = ' ';
        }
      } else {
        int i;
        for (i = 0; i < width - len_to_write; i++) {
          buffer[i] = ' ';
        }
        s21_strncpy(buffer + i, inStr, len_to_write);
      }
    } else {
      s21_strncpy(buffer, inStr, len_to_write);
    }
    result = s21_strlen(buffer);
  }
  return result;
}

int process_float(Tmarker *marker, char *buffer, va_list params) {
  int status = 1;
  char *bufferBegin = buffer;
  long double value_arg = 0.0;
  if (marker->len == 'l' || marker->len == 'L') {
    value_arg = va_arg(params, long double);
  } else {
    value_arg = va_arg(params, double);
  }
  if (marker->accuracy < 0) {
    marker->accuracy = 6;
  }
  int sign = 0;
  if (value_arg < 0) {
    sign = 1;
    value_arg *= -1;
  }
  char arr[256] = {};
  char *arrCursor = arr;
  long double intPart = 0;
  !marker->accuracy ? intPart = roundl(value_arg) : modfl(value_arg, &intPart);
  marker->sign.ifspace *= (marker->sign.ifplus || sign) ? FALSE : TRUE;
  if (marker->sign.ifspace) {
    *arrCursor = ' ';
    arrCursor++;
  }
  s21_itoa_long(marker, (long long)intPart, arrCursor);
  if (marker->accuracy || marker->sign.ifsharp) {
    s21_strcat(arrCursor, ".");
  }
  if (marker->accuracy) {
    char frac[256] = {};
    char *fracCursor = frac;
    long double ostatok_ld = value_arg - (long long)value_arg;
    int charsToWrite = marker->accuracy + 1;
    while (charsToWrite--) {
      ostatok_ld *= 10;
      if (ostatok_ld < 1) {
        *fracCursor = '0';
        fracCursor++;
      }
    }
    long long ostatok_ll = (long long)ostatok_ld;
    ostatok_ll *= ostatok_ll < 0 ? -1 : 1;
    s21_itoa_long(marker, ostatok_ll, fracCursor);
    int fraclen = s21_strlen(frac);
    while (fraclen++ < marker->accuracy) {
      s21_strcat(arrCursor, "0");
    }
    s21_strcat(arrCursor, frac);
  }
  if (marker->accuracy) {
    // сделаем округление
    int dotPlace = s21_strchr(arr, '.') - arr;
    int add = 0;
    // сначала пробегаемся по дробной части
    if (arr[dotPlace + marker->accuracy + 1] >= '5') {
      add = 1;
    }
    arr[dotPlace + marker->accuracy + 1] = '\0';
    // int i;
    for (int i = dotPlace + marker->accuracy; i > dotPlace; i--) {
      arr[i] += add;
      add = 0;
      if (arr[i] > '9') {
        arr[i] = '0';
        add = 1;
      }
    }
    // теперь смотрим на целую часть
    if (add) {
      arr[dotPlace + 1] = '0';
      for (int i = dotPlace - 1; i >= 0; i--) {
        arr[i] += add;
        add = 0;
        if (arr[i] > '9') {
          arr[i] = '0';
          add = 1;
        }
      }
    }
    // обработка переполнения старшего разряда
    if (add) {
      for (int i = s21_strlen(arr); i > 0; i--) {
        arr[i] = arr[i - 1];
      }
      arr[0] = '1';
    }
  }
  int len_arr = s21_strlen(arr);
  if (sign || marker->sign.ifplus) {
    len_arr++;
  }
  int diffirent = marker->width - len_arr;
  if (marker->sign.ifminus) {  // выравнивание по левому краю
    if (marker->sign.ifplus || sign) {
      *buffer = sign ? '-' : '+';
      buffer++;
    }
    s21_strcat(buffer, arr);
    buffer += s21_strlen(arr);
    for (int i = diffirent; i > 0; i--) {
      *buffer = ' ';
      buffer++;
    }
  } else {
    for (int i = diffirent; i > 0; i--) {
      *buffer = ' ';
      buffer++;
    }
    if (marker->sign.ifplus || sign) {
      *buffer = sign ? '-' : '+';
      buffer++;
    }
    s21_strcat(buffer, arr);
  }
  marker->fact_width = s21_strlen(bufferBegin);
  return status;
}

int process_octal(Tmarker *marker, char *buffer, va_list params) {
  char *result = buffer;
  long int o_out = va_arg(params, long int);
  char bufarr[BUF_SIZE] = "";
  char *buf = bufarr;
  // print_marker(*marker);
  if (marker->sign.ifsharp) {
    *buf = '0';
    buf++;
  }
  if (o_out < 0) {
    if (marker->len == 'l' || marker->len == 'L') {
      o_out = OCTALLONGZERO + o_out + 1;
      *buf = '1';
      buf++;
    } else {
      o_out = OCTALZERO + o_out;
      *buf = '3';
      buf++;
    }
  }
  if (marker->sign.ifzero) {
    if (marker->accuracy < marker->width) marker->accuracy = marker->width;
  }
  if (o_out == 0)
    buffer[0] = '0';
  else
    spf_dec2oct(o_out, buf);
  if (marker->sign.ifminus) {
    fillzeros1(marker, bufarr, buffer);
    s21_strcat(buffer, bufarr);
    fillspaces(marker, bufarr, buffer);
  } else {
    fillspaces(marker, bufarr, buffer);
    fillzeros1(marker, bufarr, buffer);
    s21_strcat(buffer, bufarr);
  }
  return (int)s21_strlen(result);
}

int process_hex(Tmarker *marker, char *buffer, va_list params) {
  char *result = buffer;
  bool ifupper = FALSE;
  char bufarr[BUF_SIZE] = "";
  char *buf = bufarr;
  char sharp[3] = "0x\0";
  if (marker->sign.ifsharp) {
    if (marker->key == 'X') sharp[1] = 'X';
  }
  char minus[1] = "f";
  if (marker->key == 'X') {
    ifupper = TRUE;
    minus[0] = 'F';
  }
  long int h_out = va_arg(params, long int);
  if (h_out < 0) {
    if (marker->len == 'l' || marker->len == 'L') {
      h_out = HEXLONGZERO + h_out + 1;
      s21_strcat(buf, minus);
      buf++;
    } else {
      h_out =
          HEXZERO + h_out;  // аналог шестнадцатеричной -1 в десятичной системе
      s21_strcat(buf, minus);
      buf++;
    }
  }
  if (marker->sign.ifzero) {
    if (marker->accuracy < marker->width) marker->accuracy = marker->width;
  }
  if (h_out == 0)
    buffer[0] = '0';
  else
    spf_dec2hex(h_out, buf, ifupper);
  if (marker->sign.ifsharp) s21_strcat(buffer, sharp);
  if (marker->sign.ifminus) {
    fillzeros1(marker, bufarr, buffer);
    s21_strcat(buffer, bufarr);
    fillspaces(marker, bufarr, buffer);
  } else {
    fillspaces(marker, bufarr, buffer);
    fillzeros1(marker, bufarr, buffer);
    s21_strcat(buffer, bufarr);
  }
  return (int)s21_strlen(result);
}

int process_pointer(/*Tmarker *marker,*/ char *buffer, va_list params) {
  int result = 0;
  void *p_out = va_arg(params, void *);
  // printf("ptr: %p\n", p_out);
  long int dec_addr = (long int)p_out;
  int i = 0, temp = 0;
  char revercebuf[32] = {0};
  while (dec_addr != 0) {
    temp = dec_addr % 16;
    if (temp < 10)
      temp = temp + 48;
    else
      temp = temp + 55 + 32;
    revercebuf[i++] = temp;
    dec_addr = dec_addr / 16;
  }
  buffer[0] = '0';
  buffer[1] = 'x';
  buffer += 2;
  reverse_string(revercebuf, buffer);
  if (s21_strlen(buffer) != 0) result = 1;
  return result;
}

int process_eE(Tmarker *marker, char *buffer, va_list params) {
  double ee_out = va_arg(params, double);
  double sign = 1.0;
  if (ee_out < 0) {
    buffer[0] = '-';
    buffer++;
    sign = (-1.0);
  } else {
    if (marker->sign.ifplus) {
      *buffer = '+';
      buffer++;
    }
  }
  double d = ee_out * sign;
  double gd = d;
  char exp[4] = "e+00";
  if (marker->key == 'E' || marker->key == 'G') exp[0] = 'E';
  double cc0, dc0, cc1, dc1;
  int int_digits = 0, float_digits = 0;
  dc0 = modf(d, &cc0);
  // обработка чисел с нулевой целой частью
  if (cc0 < 1) {
    exp[1] = '-';
    // printf("%lf %lf\n",cc0, dc0);
    while (dc0 <= 1.0) {
      int_digits++;
      dc0 *= 10;
    }
    dc1 = modf(dc0, &cc1);
  } else {
    while (cc0 >= 10) {
      int_digits++;
      cc0 /= 10;
      d /= 10;
    }
    dc1 = modf(d, &cc1);
  }
  // print_marker(*marker);
  cc0 = cc1;
  dc0 = dc1;
  char expend[5] = "";
  if (int_digits < 10)
    exp[3] = '0' + int_digits;
  else {
    spf_itoa(int_digits, expend);
    exp[2] = expend[0];
    exp[3] = expend[1];
  }

  dc1 = dc0 = round(dc0 * 1e6);
  while (dc1 >= 1) {
    float_digits++;
    dc1 /= 10;
  }
  float_digits = 6 - float_digits;
  if (int_digits + 1 > 5 || (marker->key == 'e' || marker->key == 'E')) {
    char int_str[BUF_SIZE] = "";
    spf_itoa((int)cc0, int_str);
    s21_strcat(buffer, int_str);
    s21_strcat(buffer, ".");
    char zeros[BUF_SIZE] = "";
    if (float_digits > 0) fillzeros(zeros, float_digits);
    s21_strcat(buffer, zeros);
    char float_str[BUF_SIZE] = "";
    spf_itoa((int)dc0, float_str);
    s21_strcat(buffer, float_str);
    exp[4] = '\0';
    s21_strcat(buffer, exp);
  } else {
    // Обработка строки числа по формату gG
    char int_str[BUF_SIZE] = "";
    spf_itoa((int)gd, int_str);
    s21_strcat(buffer, int_str);
    float_digits = 6 - (int_digits + 1);
    if ((int)gd == 0) {
      float_digits += 2;
      s21_strcat(buffer, "0");
    }
    dc0 = modf(gd, &cc0);
    dc0 = round(dc0 * pow(10, float_digits));
    char float_str[BUF_SIZE] = "";
    spf_itoa((int)dc0, float_str);
    if (dc0 != 0) {
      s21_strcat(buffer, ".");
      char zeros[BUF_SIZE] = "";
      fillzeros(zeros, 6 - int_digits - s21_strlen(float_str) - 1);
      s21_strcat(buffer, zeros);
      s21_strcat(buffer, float_str);
    }
    // Конец обработки gG
  }
  int result = 0;
  if (s21_strlen(buffer) != 0) result = 1;
  return result;
}
