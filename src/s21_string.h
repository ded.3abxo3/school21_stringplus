#ifndef S21_STRING_H_
#define S21_STRING_H_

/* ----- ИНКЛЮДЫ ----- */
#include <limits.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

/* средства для перебора аргументов функции / va_start(); va_arg(); va_end(); */
#include <stdarg.h>

/* ----- ДЕФАЙНЫ, ТИПЫ, КОНСТАНТЫ и Т.П. ----- */
#define S21_NULL ((void *)0)
#define BUF_SIZE 2560
#define EXP 1E-6
#define OCTALZERO 1073741824
#define OCTALLONGZERO 9223372036854775807
#define HEXZERO 268435456
#define HEXLONGZERO 1152921504606846975

typedef unsigned long s21_size_t;
typedef unsigned char s21_uchar;
// перечисляемый тип enum по умолчанию начинается с нуля с шагом 1, т.е. false =
// 0, true = 1
typedef enum { FALSE, TRUE } bool;

/* МАКРОСЫ ПОПАДАНИЯ В ДИАПАЗОНЫ */
#define issign_macro(sym) \
  ((sym) == '-' || (sym) == '+' || (sym) == ' ' || (sym) == '#' || (sym) == '0')

#define isdec_macro(sym) ((sym) >= '0' && (sym) <= '9')

#define ishex_macro(sym)                                             \
  (((sym) >= '0' && (sym) <= '9') || (sym) == 'a' || (sym) == 'b' || \
   (sym) == 'c' || (sym) == 'd' || (sym) == 'e' || (sym) == 'f' ||   \
   (sym) == 'A' || (sym) == 'B' || (sym) == 'C' || (sym) == 'D' ||   \
   (sym) == 'E' || (sym) == 'F')

#define isformat_macro(sym)                                        \
  ((sym) == 'c' || (sym) == 'd' || (sym) == 'i' || (sym) == 'e' || \
   (sym) == 'E' || (sym) == 'f' || (sym) == 'g' || (sym) == 'G' || \
   (sym) == 'o' || (sym) == 's' || (sym) == 'u' || (sym) == 'x' || \
   (sym) == 'X' || (sym) == 'p' || (sym) == 'n' || (sym) == '%' || \
   (sym) == '*')

#define islen_macro(sym) ((sym) == 'h' || (sym) == 'l' || (sym) == 'L')

#define isdot_macro(sym) ((sym) == '.')

#define isint_macro(sym) ((sym) == 'd' || (sym) == 'i' || (sym) == 'u')

typedef struct smarker {
  struct ssign {
    bool ifplus;
    bool ifminus;
    bool ifspace;
    bool ifsharp;
    bool ifzero;
  } sign;
  char key;        // спецификатор
  char len;        // число длинное (l) или короткое (h)
  int width;       // ширина поля вывода
  int accuracy;    // точность (знаков после точки)
  int fact_width;  // фактическая ширина поля вывода
  bool ifdot;
} Tmarker;

typedef struct arguments {
  bool star;     // звездочка есть или нет
  char format;   // спецификатор \0 def
  int width;     // ширина поля -1 def
  char len;      // длина h l L \0 def
  bool success;  // статус считывания
} TArgs;

/* ----- СТАНДАРТНЫЕ ФУНКЦИИ ----- */
void *s21_memchr(const void *str, int c,
                 size_t n);  // Выполняет поиск первого вхождения символа c
                             // (беззнаковый тип) в первых n байтах строки, на
                             // которую указывает аргумент str. |
int s21_memcmp(const void *str1, const void *str2,
               size_t n);  // Сравнивает первые n байтов str1 и str2. |
void *s21_memcpy(void *dest, const void *src,
                 size_t n);  //| Копирует n символов из src в dest. |
void *s21_memmove(void *dest, const void *src,
                  size_t n);  //| Еще одна функция для копирования n символов из
                              // str2 в str1. |
void *s21_memset(
    void *str, int c,
    size_t n);  //| Копирует символ c (беззнаковый тип) в первые n символов
                //строки, на которую указывает аргумент str. |

char *s21_strcat(
    char *dest,
    const char *src);  //| Добавляет строку, на которую указывает src, в конец
                       //строки, на которую указывает dest. |
char *s21_strncat(
    char *dest, const char *src,
    size_t n);  //| Добавляет строку, на которую указывает src, в конец строки,
                //на которую указывает dest, длиной до n символов. |
char *s21_strchr(
    const char *str,
    int c);  //| Выполняет поиск первого вхождения символа c (беззнаковый тип) в
             //строке, на которую указывает аргумент str. |
int s21_strcmp(
    const char *str1,
    const char *str2);  //| Сравнивает строку, на которую указывает str1, со
                        //строкой, на которую указывает str2. |
int s21_strncmp(
    const char *str1, const char *str2,
    size_t n);  //| Сравнивает не более первых n байтов str1 и str2. |
char *s21_strcpy(
    char *dest,
    const char *src);  //| Копирует строку, на которую указывает src, в dest. |
char *s21_strncpy(char *dest, const char *src,
                  size_t n);  //| Копирует до n символов из строки, на которую
                              //указывает src, в dest. |
size_t s21_strcspn(
    const char *str1,
    const char *str2);  //| Вычисляет длину начального сегмента str1, который
                        //полностью состоит из символов, не входящих в str2. |
char *s21_strerror(
    int errnum);  //| Выполняет поиск во внутреннем массиве номера ошибки errnum
                  //и возвращает указатель на строку с сообщением об ошибке.
                  //Нужно объявить макросы, содержащие массивы сообщений об
                  //ошибке для операционных систем mac и linux. Описания ошибок
                  //есть в оригинальной библиотеке. Проверка текущей ОС
                  //осуществляется с помощью директив.|
size_t s21_strlen(const char *str);  //| Вычисляет длину строки str, не включая
                                     //завершающий нулевой символ. |
char *s21_strpbrk(
    const char *str1,
    const char *str2);  //| Находит первый символ в строке str1, который
                        //соответствует любому символу, указанному в str2. |
char *s21_strrchr(
    const char *str,
    int c);  //| Выполняет поиск последнего вхождения символа c (беззнаковый
             //тип) в строке, на которую указывает аргумент str. |
size_t s21_strspn(
    const char *str1,
    const char *str2);  //| Вычисляет длину начального сегмента str1, который
                        //полностью состоит из символов str2. |
char *s21_strstr(
    const char *haystack,
    const char *needle);  //| Находит первое вхождение всей строки needle (не
                          //включая завершающий нулевой символ), которая
                          //появляется в строке haystack. |
char *s21_strtok(char *str, const char *delim);  //| Разбивает строку str на ряд
                                                 //токенов, разделенных delim. |

void *s21_insert(const char *src, const char *str, s21_size_t start_pos);
void *s21_to_lower(const char *str);
void *s21_to_upper(const char *str);
void *s21_trim(const char *src, const char *trim_chars);
/* ------ ВСПОМОГАТЕЛЬНЫЕ ФУНКЦИИ ------- */
// void add_symbol (char *str, const char *format);
// char char_in_pos(char *str, int pos);
void reverse_string(char *fromstr, char *tostr);
void spf_itoa(int number, char *buffer);
void spf_dec2oct(long int o_out, char *buff);
void spf_dec2hex(long int o_out, char *buff, bool ifupper);
void fillzeros(char *str, int zeros);
int fillspaces(Tmarker *marker, char *bufarr, char *buffer);
int fillzeros1(Tmarker *marker, char *bufarr, char *buffer);
long long s21_atoi(char *x);
void s21_itoa_long(Tmarker *marker, long long number, char *str);
int digits_count(int number);
/* ------------------------------------ */

/* ----- РЕАЛИЗАЦИЯ SPRINTF ----- */

int s21_sprintf(char *str, const char *format, ...);

int process_string(char *str, const char *format, va_list params);
Tmarker marker_init();
int format_parsing(const char **format, Tmarker *marker, va_list params);
void set_sign(char sym, Tmarker *marker);
int set_width_acc(const char **format, va_list params, char mode,
                  Tmarker *marker);
char *find_digits(char *dest, const char *src);
int process_key(Tmarker *marker, char *buffer, va_list params);
int process_char(Tmarker *marker, char *buffer, va_list params);
int process_int(Tmarker *marker, char *buffer, va_list params);
int process_str(Tmarker *marker, char *buffer, va_list params);
int process_float(Tmarker *marker, char *buffer, va_list params);
int process_octal(Tmarker *marker, char *buffer, va_list params);
int process_hex(Tmarker *marker, char *buffer, va_list params);
int process_pointer(/*Tmarker *marker,*/ char *buffer, va_list params);
int process_eE(Tmarker *marker, char *buffer, va_list params);
int process_gG(Tmarker *marker, char *buffer, va_list params);
// тестирование
// void print_marker(Tmarker data);
/*---------------------------------------*/

/* ----- РЕАЛИЗАЦИЯ SSCANF ----- */

int s21_sscanf(const char *buffer, const char *format, ...);

int read_d(va_list valist, const char *str, TArgs *args);
void read_d_push_value(va_list valist, TArgs *args, long int *result);
int read_i(va_list valist, const char *str, TArgs *args);
int read_f(va_list valist, const char *str, TArgs *args, int *total_count);
void read_f_push_value(va_list valist, TArgs *args, long double *result);
int read_e(const char *str, long double *result, int *sym_counter, TArgs *args);
int read_u(va_list valist, const char *str, TArgs *args);
int read_o(va_list valist, const char *str, TArgs *args);
int read_p(va_list valist, const char *str, TArgs *args);
int read_x(va_list valist, const char *str, TArgs *args);
long parse_x2dec(const char *str, TArgs *args, int *sym_counter);
int read_c(va_list valist, const char *str, TArgs *args);
int read_s(va_list valist, const char *str, TArgs *args);
long s21_sscanf_atoi(const char *str, TArgs *args, int *sym_counter);
// тестирование
// void print_smarker(TArgs *args);
/*---------------------------------------*/

#endif  // S21_STRING_H_