#include "s21_string.h"

// переворачивание строки
void reverse_string(char *fromstr, char *tostr) {
  int numberlen = s21_strlen(fromstr) - 1;
  for (int i = numberlen; i >= 0; i--) tostr[numberlen - i] = fromstr[i];
}

// конвертация целого числа в строку
void spf_itoa(int number, char *buffer) {
  int absnumber = abs(number);
  int i = 0;
  char revercenum[32] = "";  // запись цифр в обратном порядке
  while (absnumber > 0) {
    revercenum[i] = '0' + absnumber - (int)absnumber / 10 * 10;
    absnumber = (int)(absnumber / 10);
    i++;
  }
  reverse_string(revercenum, buffer);
}

// конвертация десятичного числа в восьмеричное
void spf_dec2oct(long int o_out, char *buff) {
  int i = 0;
  char revercebuf[32] = {0};
  while (o_out != 0) {
    // заполнение массива цифрами в обратном порядке
    revercebuf[i] = '0' + (o_out % 8);
    o_out = o_out / 8;
    i++;
  }
  reverse_string(revercebuf, buff);
}

// конвертация десятичного числа в шестнадцатеричное
void spf_dec2hex(long int h_out, char *buff, bool ifupper) {
  int i = 0, temp = 0;
  int shift = 0;
  if (!ifupper) shift = 32;
  char revercebuf[32] = {0};
  while (h_out != 0) {
    temp = h_out % 16;
    if (temp < 10)
      temp = temp + 48;
    else
      temp = temp + 55 + shift;
    revercebuf[i++] = temp;
    h_out = h_out / 16;
  }
  reverse_string(revercebuf, buff);
}

void fillzeros(char *str, int zeros) {
  for (int i = 0; i < zeros; i++) s21_strcat(str, "0");
}

int fillspaces(Tmarker *marker, char *bufarr, char *buffer) {
  int spcs = 0;
  if (marker->width > 0) spcs = marker->width - (int)s21_strlen(bufarr);
  int acrcy = 0;
  if (marker->accuracy > 0)
    acrcy = abs(marker->accuracy - (int)s21_strlen(bufarr));
  if (marker->width > 0)
    for (int i = 0; i < spcs - acrcy; i++) s21_strcat(buffer, " ");
  return spcs - acrcy;
}

int fillzeros1(Tmarker *marker, char *bufarr, char *buffer) {
  int acrcy = 0;
  if (marker->accuracy > 0)
    acrcy = abs(marker->accuracy - (int)s21_strlen(bufarr));
  if (marker->accuracy > 0)
    for (int i = 0; i < acrcy; i++) s21_strcat(buffer, "0");
  return acrcy;
}

long long s21_atoi(char *x) {
  int sign = 1;
  if (x[0] == '-') {
    x++;
    sign *= -1;
  }
  long long numb = 0;
  for (int i = 0; x[i] >= '0' && x[i] <= '9'; i++) {
    numb *= 10;
    numb += x[i] - '0';
  }
  return numb * sign;
}

void s21_itoa_long(Tmarker *marker, long long number, char *str) {
  int cnt = 0;
  unsigned long tempnum = 0;
  bool iful = (marker->len == 'l' || marker->len == 'L');
  if (iful) {
    tempnum = number;
  } else {
    tempnum = (long long)number;
    if (number < 0) {
      tempnum *= -1;
    } else if (tempnum == 0) {
      str[cnt] = '0';
      cnt++;
    }
  }
  while (tempnum > 0) {
    str[cnt] = tempnum % 10 + '0';
    cnt++;
    tempnum /= 10;
  }
  if (number < 0 && !iful) {
    str[cnt] = '-';
    cnt++;
  }
  str[cnt] = '\0';
  int strlen = s21_strlen(str) - 1;
  for (int i = 0; i < strlen; i++, strlen--) {
    char temp = str[i];
    str[i] = str[strlen];
    str[strlen] = temp;
  }
  if (number == 0 && !s21_strlen(str)) s21_strcat(str, "0\0");
}

/* ТЕСТИРОВАНИЕ */
/*
void print_marker(Tmarker data) {
    printf("[-] %d | ", data.sign.ifminus);
    printf("[+] %d | ", data.sign.ifplus);
    printf("[ ] %d | ", data.sign.ifspace);
    printf("[#] %d | ", data.sign.ifsharp);
    printf("[0] %d\n", data.sign.ifzero);
    printf("[key] %c | ", data.key);
    printf("[len] %c | ", data.len);
    printf("[width] %d | ", data.width);
    printf("[accuracy] %d\n", data.accuracy);
    printf("[dot] %d\n", data.ifdot);
    printf("------------\n");
}*/
