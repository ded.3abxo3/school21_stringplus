#include "s21_string.h"

/* основная идея - это КАКИМ ОБРАЗОМ считать аргумент
   TYPE argument = va_arg(valist, TYPE *);
   вся фишка именно в установке правильных типов */

int s21_sscanf(const char *str, const char *format, ...) {
  int result = 0;
  long sym_count = 0;  // общий счетчик считываний
  va_list params;
  va_start(params, format);

  while (*format) {
    if (*format == '%') format++;
    long current_sym_count = 0;  // счетчик текущих считываний
    TArgs args = {FALSE, '\0', -1, '\0', FALSE};
    while (isformat_macro(*format) || islen_macro(*format) ||
           isdec_macro(*format) || isdot_macro(*format) ||
           issign_macro(*format)) {
      switch (*format) {
        case '0' ... '9':
          if (args.width == -1)
            args.width = (*format - '0');
          else
            args.width = args.width * 10 + (*format - '0');
          break;
        case 'h':
        case 'l':
        case 'L':
          args.len = *format;
          break;
        case 'd':
          current_sym_count += read_d(params, str, &args);
          break;
        case 'c':
          current_sym_count += read_c(params, str, &args);
          break;
        case 's':
          current_sym_count += read_s(params, str, &args);
          break;
        case 'e':
        case 'E':
        case 'g':
        case 'G':
        case 'f':
          current_sym_count += read_f(params, str, &args, &result);
          break;
        case 'u':
          current_sym_count += read_u(params, str, &args);
          break;
        case 'o':
          current_sym_count += read_o(params, str, &args);
          break;
        case 'X':
        case 'x':
          current_sym_count += read_x(params, str, &args);
          break;
        case 'i':
          current_sym_count += read_i(params, str, &args);
          break;
        case 'p':
          current_sym_count += read_p(params, str, &args);
          break;
        case '%':
          while (*str == ' ' || *str == '%') {
            args.success = -1;
            sym_count++;
            str++;  // перематываем пробелы и %
          }
          break;
      }
      args.format = *format;
      format++;
    }
    format++;
    if (!args.success) break;  // Завершаем цикл с ошибкой 0
    if (args.star)
      args.success = 0;  // Звездочка (*) после знака процента подавляет
                         // назначение следующего поля ввода

    /* ОТЛАДКА */
    // if (args.success) print_smarker(&args);
    // printf("Symbols count: %ld\n", sym_count);
    /* КОНЕЦ ОТЛАДКИ */

    // result+=n_succes;
    result += args.success;
    str += current_sym_count;  // перематываем строку на кол-во обработанных
                               // символов
    sym_count += current_sym_count;
  }
  va_end(params);
  return result ? result : -1;  // -1 если формат не был задан
}

/* ФУНКЦИИ */

// обработка целого десятичного числа
int read_d(va_list valist, const char *str, TArgs *args) {
  int sym_counter = 0;
  long result = s21_sscanf_atoi(str, args, &sym_counter);
  if (!args->star) read_d_push_value(valist, args, &result);
  return sym_counter;
}

// обработка флагов длины для целого числа
void read_d_push_value(va_list valist, TArgs *args, long int *result) {
  if (args->len == '\0') {
    int *destination = va_arg(valist, int *);
    *destination = (int)*result;
  } else if (args->len == 'h') {
    short *destination = va_arg(valist, short *);
    *destination = (short)*result;
  } else if (args->len == 'l') {
    long *destination = va_arg(valist, long *);
    *destination = (long)*result;
  }
}

// обработка флага i (любое целое число в любой системе)
int read_i(va_list valist, const char *str, TArgs *args) {
  int sym_counter = 0;
  while (*str == ' ') {
    str++;
    sym_counter++;
  }
  if (*str == '0') {
    str++;
    sym_counter++;
    if (*str == 'x') {  // обработка hex 0хfab13
      sym_counter += read_x(valist, str, args);
    } else {
      // обработка oct если число формата 0654
      sym_counter += read_o(valist, str, args);
    }
  } else if (isdec_macro(*str) || *str == '-' || *str == '+') {
    // обработка dec
    sym_counter += read_d(valist, str, args);
  }
  return sym_counter;
}

// обработка флага p (указатель)
int read_p(va_list valist, const char *str, TArgs *args) {
  int sym_counter = 0;
  long decimal = parse_x2dec(str, args, &sym_counter);
  if (!args->star) {
    void **destination =
        va_arg(valist, void **);  // указатель на указатель на аргумент
    *destination = (void *)(0x0 + decimal);
    /* значением аргумента устанавливаем указатель типа void*
    получаемый путем сложения 0х0 и десятичного числа,
    полученного из обработки строки */
  }
  return sym_counter;
}

// обработка флага x (число hex)
int read_x(va_list valist, const char *str, TArgs *args) {
  int sym_counter = 0;
  long decimal = parse_x2dec(str, args, &sym_counter);
  if (args->star == 0) read_d_push_value(valist, args, &decimal);
  return sym_counter;
}

// конвертация hex -> decimal
long parse_x2dec(const char *str, TArgs *args, int *sym_counter) {
  long decimal = 0;
  int sign = 1;
  while (*str == ' ' || *str == '-' || *str == '+' || *str == '0' ||
         *str == 'x' || *str == 'X') {
    if (*str == '-') sign = -1;
    str++;
    (*sym_counter)++;
    if (*str == 'x' || *str == 'X') {
      str++;
      break;
    }
  }
  char hex_num[50] = {'\0'};
  int i = 0;
  while ((*str && *str != ' ' && *str != '\n' && args->width--) ||
         ishex_macro(hex_num[i])) {
    (*sym_counter)++;
    hex_num[i++] = *str++;
  }
  int len = s21_strlen(hex_num);
  int temp = 0;
  for (i = 0; hex_num[i] != '\0'; i++) {
    if (isdec_macro(hex_num[i])) {
      temp = hex_num[i] - 48;
      args->success = 1;
    } else if (hex_num[i] >= 'a' && hex_num[i] <= 'f') {
      temp = hex_num[i] - 87;
      args->success = 1;
    } else if (hex_num[i] >= 'A' && hex_num[i] <= 'F') {
      temp = hex_num[i] - 55;
      args->success = 1;
    }
    decimal += temp * pow(16, --len);
  }
  return decimal *= sign;
}

// обработка флага о (octal)
int read_o(va_list valist, const char *str, TArgs *args) {
  int sym_counter = 0;
  char buf[BUF_SIZE] = "";
  char *pbuf = buf;
  s21_strcpy(buf, str);
  for (int i = 0; i < (int)s21_strlen(buf); i++)
    if (buf[i] == '8' || buf[i] == '9') buf[i] = '\0';

  long octal = s21_sscanf_atoi(buf, args, &sym_counter);
  long decimal = 0;
  while (*pbuf == ' ' || *pbuf == '-' || *pbuf == '+' || *pbuf == '0') pbuf++;
  int i = 0;
  while (octal != 0 && args->width--) {
    decimal = decimal + (octal % 10) * pow(8, i++);
    octal = octal / 10;
  }
  if (args->star == 0) read_d_push_value(valist, args, &decimal);
  return sym_counter;
}

// u - то же самое, что и d,  только вместо int -> unsigned int (не допускает
// отриц числа)
int read_u(va_list valist, const char *str, TArgs *args) {
  int sym_counter = 0;
  if (args->star == 0) {
    if (args->len == '\0') {
      unsigned int *destination = va_arg(valist, unsigned int *);
      long temp = s21_sscanf_atoi(str, args, &sym_counter);
      *destination = (unsigned int)temp;
    } else if (args->len == 'h') {
      unsigned short *destination = va_arg(valist, unsigned short *);
      long temp = s21_sscanf_atoi(str, args, &sym_counter);
      *destination = (unsigned short)temp;
    } else if (args->len == 'l') {
      unsigned long *destination = va_arg(valist, unsigned long *);
      long temp = s21_sscanf_atoi(str, args, &sym_counter);
      *destination = (unsigned long)temp;
    }
  } else {
    s21_sscanf_atoi(str, args, &sym_counter);
  }
  return sym_counter;
}

// обработка флагов f и eE
int read_f(va_list valist, const char *str, TArgs *args, int *total_count) {
  int sym_counter = 0;
  long double left_from_dot_or_e = s21_sscanf_atoi(str, args, &sym_counter);
  // sym_counter получает кол-во цифр в левой части числа до . или e
  long double result = left_from_dot_or_e;

  int sign = 1;
  const char *str_temp = str;
  while (*str_temp == ' ') str_temp++;
  if (*str_temp == '-') sign = -1;

  str += sym_counter;  // перемотка указателя до . или е
  switch (*str) {
    case '.':
      str++;
      int symbols_to_right_of_dot = 0;
      long double right_from_dot =
          s21_sscanf_atoi(str, args, &symbols_to_right_of_dot);
      sym_counter +=
          symbols_to_right_of_dot + 1;  // +1 - включаем точку в подсчет

      for (int n = symbols_to_right_of_dot; n > 0; n--) right_from_dot /= 10;

      if (!left_from_dot_or_e && !right_from_dot && sign == -1)
        result = -0.0;
      else if (sign == -1)
        result -= right_from_dot;
      else
        result += right_from_dot;

      str += symbols_to_right_of_dot;  // перемотка в конец числа после . или
                                       // после е
      break;
    case 'e':
    case 'E':
      if (args->width == 1) {
        read_f_push_value(valist, args, &result);
        args->success = 0;
        (*total_count)++;
        break;
      }
      str += read_e(str, &result, &sym_counter, args);
      break;
  }
  if (*str == 'e' || *str == 'E') read_e(str, &result, &sym_counter, args);

  if (args->star == 0 && args->success)
    read_f_push_value(valist, args, &result);
  return sym_counter;
}

// вызов из read_f в случае нахождения символов e E
int read_e(const char *str, long double *result, int *sym_counter,
           TArgs *args) {
  str++;  // проматываем e
  args->width--;
  int e_sym_counter = 1;

  // числовая часть экспоненты (цифры после e+ e-)
  int n = s21_sscanf_atoi(str, args, &e_sym_counter);

  if (*str == '+' || isdec_macro(*str)) {
    while (n--) *result *= 10.0;
  } else if (*str == '-') {
    while (n++) *result /= 10.0;
  }

  *sym_counter += e_sym_counter;
  return e_sym_counter;
}

// обработка длины для обработчика read_f
void read_f_push_value(va_list valist, TArgs *args, long double *result) {
  if (args->len == 'l') {
    double *destination = va_arg(valist, double *);
    *destination = (double)*result;
  } else if (args->len == 'L') {
    long double *destination = va_arg(valist, long double *);
    *destination = (long double)*result;
  } else {
    float *destination = va_arg(valist, float *);
    *destination = (float)*result;
  }
}

// обработка строки
int read_s(va_list valist, const char *str, TArgs *args) {
  if (args->width > (int)s21_strlen(str)) args->width = (int)s21_strlen(str);
  int sym_counter = 0;
  while (*str == ' ' || *str == '\n') {  // перематываем пробелы и концы строк
    str++;
    sym_counter++;
  }
  // if (!args->star) {
  if (args->len == 'l') {
    wchar_t *destination = va_arg(valist, wchar_t *);
    while (*str && *str != ' ' && *str != '\n' && args->width--) {
      sym_counter++;
      *destination++ = *str++;
      args->success = 1;
    }
    *destination = '\0';
  } else {
    char *destination = va_arg(valist, char *);
    while (*str && *str != ' ' && *str != '\n' && args->width--) {
      sym_counter++;
      *destination++ = *str++;
      args->success = 1;
    }
    *destination = '\0';
  }
  /*} else {
    while (*str && *str != ' ' && *str != '\n' && args->width--) {
      str++;
      sym_counter++;
      args->success = 1;
    }
  }*/

  return sym_counter;
}

// обработка символа
int read_c(va_list valist, const char *str, TArgs *args) {
  int sym_counter = 0;
  if (args->width > 0) {
    sym_counter += read_s(valist, str, args);
  } else if (args->star == 0) {
    if (args->len == 'l') {
      wchar_t *destination = va_arg(valist, wchar_t *);
      *destination = *str;
    } else {
      char *destination = va_arg(valist, char *);
      *destination = *str;
    }
    sym_counter++;
    args->success = 1;
  }
  return sym_counter;
}

// строка в число с удалением всех ненужных символов, но с подсчетом их
long s21_sscanf_atoi(const char *str, TArgs *args, int *sym_counter) {
  long res = 0;
  int sign = 1;
  while (*str == ' ' || *str == '-' || *str == '+') {
    if (*str == '-') {
      sign = -1;
      args->width--;
    } else if (*str == '+') {
      args->width--;
    }
    str++;
    (*sym_counter)++;
  }
  while (isdec_macro(*str) && args->width--) {
    res = res * 10 + (*str - '0');
    str++;
    (*sym_counter)++;
    args->success = 1;
  }
  return res * sign;
}

/* ТЕСТИРОВАНИЕ */
/*
void print_smarker(TArgs *args) {
  printf("[*] %d [format] %c [width] %d [len] %c [sussess] %d\n", args->star,
         args->format, args->width, args->len, args->success);
}*/
