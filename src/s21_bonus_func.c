#include "s21_string.h"

void *s21_insert(const char *src, const char *str, s21_size_t start_pos) {
  char *buffer = S21_NULL;
  if (src != S21_NULL && str != S21_NULL) {
    s21_size_t src_len = s21_strlen(src);
    s21_size_t str_len = s21_strlen(str);
    if (src_len >= start_pos) {
      buffer = calloc(src_len + str_len + 1, sizeof(char));
      if (buffer != S21_NULL) {
        s21_strcpy(buffer, src);
        s21_strcpy(buffer + start_pos, str);
        s21_strcpy(buffer + str_len + start_pos, src + start_pos);
      }
    }
  }
  return buffer;
}

void *s21_to_lower(const char *str) {
  s21_size_t length = s21_strlen(str);
  char *result = malloc((length + 1) * sizeof(char));
  s21_size_t i = 0;
  while (i <= length) {
    if (str[i] >= 'A' && str[i] <= 'Z') {
      result[i] = str[i] + 32;
    } else {
      result[i] = str[i];
    }
    i++;
  }
  return result;
}

void *s21_to_upper(const char *str) {
  s21_size_t length = s21_strlen(str);
  s21_size_t i = 0;
  char *result = malloc((length + 1) * sizeof(char));
  if (result == S21_NULL) exit(1);
  while (i <= length) {
    if (str[i] >= 'a' && str[i] <= 'z') {
      result[i] = str[i] - 32;
    } else {
      result[i] = str[i];
    }
    i++;
  }
  return result;
}

void *s21_trim(const char *src, const char *trim_chars) {
  const char common[] = "\t\n";  //удаляемые символы по-умолчанию
  s21_size_t remove_left = 0, remove_right = 0, size_source = 0,
             size_result = 0;
  if (src != S21_NULL) {
    size_source = s21_strlen(src);
  }
  if (trim_chars == S21_NULL || s21_strlen(trim_chars) == 0) {
    trim_chars = common;
  }
  // подсчет символов для удаления справа
  for (s21_size_t i = size_source; i > 0 && s21_strchr(trim_chars, src[i - 1]);
       i--) {
    remove_right++;
  }
  size_result = size_source - remove_right;
  // подсчет символов для удаления слева
  for (s21_size_t i = 0; i < size_result && s21_strchr(trim_chars, src[i]);
       i++) {
    remove_left++;
  }
  size_result -= remove_left;
  // копируем символы в строку вывода, пропуская левые символы
  char *res = (char *)calloc(size_result + 1, sizeof(char));
  if (res != S21_NULL) {
    for (s21_size_t i = remove_left; i < size_source - remove_right; i++) {
      res[i - remove_left] = src[i];
    }
  }
  return res;
}