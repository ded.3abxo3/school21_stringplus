#include "s21_string.h"

/* geneviej */
#ifndef ERRLIST_H
#define ERRLIST_H

#if defined(__APPLE__)
#define ERRLIST_LEN 107
#define ERRLIST                                                                \
  {                                                                            \
    "Undefined error: 0", "Operation not permitted",                           \
        "No such file or directory", "No such process",                        \
        "Interrupted system call", "Input/output error",                       \
        "Device not configured", "Argument list too long",                     \
        "Exec format error", "Bad file descriptor", "No child processes",      \
        "Resource deadlock avoided", "Cannot allocate memory",                 \
        "Permission denied", "Bad address", "Block device required",           \
        "Resource busy", "File exists", "Cross-device link",                   \
        "Operation not supported by device", "Not a directory",                \
        "Is a directory", "Invalid argument", "Too many open files in system", \
        "Too many open files", "Inappropriate ioctl for device",               \
        "Text file busy", "File too large", "No space left on device",         \
        "Illegal seek", "Read-only file system", "Too many links",             \
        "Broken pipe", "Numerical argument out of domain", "Result too large", \
        "Resource temporarily unavailable", "Operation now in progress",       \
        "Operation already in progress", "Socket operation on non-socket",     \
        "Destination address required", "Message too long",                    \
        "Protocol wrong type for socket", "Protocol not available",            \
        "Protocol not supported", "Socket type not supported",                 \
        "Operation not supported", "Protocol family not supported",            \
        "Address family not supported by protocol family",                     \
        "Address already in use", "Can't assign requested address",            \
        "Network is down", "Network is unreachable",                           \
        "Network dropped connection on reset",                                 \
        "Software caused connection abort", "Connection reset by peer",        \
        "No buffer space available", "Socket is already connected",            \
        "Socket is not connected", "Can't send after socket shutdown",         \
        "Too many references: can't splice", "Operation timed out",            \
        "Connection refused", "Too many levels of symbolic links",             \
        "File name too long", "Host is down", "No route to host",              \
        "Directory not empty", "Too many processes", "Too many users",         \
        "Disc quota exceeded", "Stale NFS file handle",                        \
        "Too many levels of remote in path", "RPC struct is bad",              \
        "RPC version wrong", "RPC prog. not avail", "Program version wrong",   \
        "Bad procedure for program", "No locks available",                     \
        "Function not implemented", "Inappropriate file type or format",       \
        "Authentication error", "Need authenticator", "Device power is off",   \
        "Device error", "Value too large to be stored in data type",           \
        "Bad executable (or shared library)", "Bad CPU type in executable",    \
        "Shared library version mismatch", "Malformed Mach-o file",            \
        "Operation canceled", "Identifier removed",                            \
        "No message of desired type", "Illegal byte sequence",                 \
        "Attribute not found", "Bad message", "EMULTIHOP (Reserved)",          \
        "No message available on STREAM", "ENOLINK (Reserved)",                \
        "No STREAM resources", "Not a STREAM", "Protocol error",               \
        "STREAM ioctl timeout", "Operation not supported on socket",           \
        "Policy not found", "State not recoverable", "Previous owner died",    \
        "Interface output queue is full"                                       \
  }

#elif defined(__linux__)
#define ERRLIST_LEN 133
#define ERRLIST                                                                \
  {                                                                            \
    "Success", "Operation not permitted", "No such file or directory",         \
        "No such process", "Interrupted system call", "Input/output error",    \
        "No such device or address", "Argument list too long",                 \
        "Exec format error", "Bad file descriptor", "No child processes",      \
        "Resource temporarily unavailable", "Cannot allocate memory",          \
        "Permission denied", "Bad address", "Block device required",           \
        "Device or resource busy", "File exists", "Invalid cross-device link", \
        "No such device", "Not a directory", "Is a directory",                 \
        "Invalid argument", "Too many open files in system",                   \
        "Too many open files", "Inappropriate ioctl for device",               \
        "Text file busy", "File too large", "No space left on device",         \
        "Illegal seek", "Read-only file system", "Too many links",             \
        "Broken pipe", "Numerical argument out of domain",                     \
        "Numerical result out of range", "Resource deadlock avoided",          \
        "File name too long", "No locks available",                            \
        "Function not implemented", "Directory not empty",                     \
        "Too many levels of symbolic links", "(null)",                         \
        "No message of desired type", "Identifier removed",                    \
        "Channel number out of range", "Level 2 not synchronized",             \
        "Level 3 halted", "Level 3 reset", "Link number out of range",         \
        "Protocol driver not attached", "No CSI structure available",          \
        "Level 2 halted", "Invalid exchange", "Invalid request descriptor",    \
        "Exchange full", "No anode", "Invalid request code", "Invalid slot",   \
        "(null)", "Bad font file format", "Device not a stream",               \
        "No data available", "Timer expired", "Out of streams resources",      \
        "Machine is not on the network", "Package not installed",              \
        "Object is remote", "Link has been severed", "Advertise error",        \
        "Srmount error", "Communication error on send", "Protocol error",      \
        "Multihop attempted", "RFS specific error", "Bad message",             \
        "Value too large for defined data type", "Name not unique on network", \
        "File descriptor in bad state", "Remote address changed",              \
        "Can not access a needed shared library",                              \
        "Accessing a corrupted shared library",                                \
        ".lib section in a.out corrupted",                                     \
        "Attempting to link in too many shared libraries",                     \
        "Cannot exec a shared library directly",                               \
        "Invalid or incomplete multibyte or wide character",                   \
        "Interrupted system call should be restarted", "Streams pipe error",   \
        "Too many users", "Socket operation on non-socket",                    \
        "Destination address required", "Message too long",                    \
        "Protocol wrong type for socket", "Protocol not available",            \
        "Protocol not supported", "Socket type not supported",                 \
        "Operation not supported", "Protocol family not supported",            \
        "Address family not supported by protocol", "Address already in use",  \
        "Cannot assign requested address", "Network is down",                  \
        "Network is unreachable", "Network dropped connection on reset",       \
        "Software caused connection abort", "Connection reset by peer",        \
        "No buffer space available",                                           \
        "Transport endpoint is already connected",                             \
        "Transport endpoint is not connected",                                 \
        "Cannot send after transport endpoint shutdown",                       \
        "Too many references: cannot splice", "Connection timed out",          \
        "Connection refused", "Host is down", "No route to host",              \
        "Operation already in progress", "Operation now in progress",          \
        "Stale file handle", "Structure needs cleaning",                       \
        "Not a XENIX named type file", "No XENIX semaphores available",        \
        "Is a named type file", "Remote I/O error", "Disk quota exceeded",     \
        "No medium found", "Wrong medium type", "Operation canceled",          \
        "Required key not available", "Key has expired",                       \
        "Key has been revoked", "Key was rejected by service", "Owner died",   \
        "State not recoverable", "Operation not possible due to RF-kill",      \
        "Memory page has hardware error"                                       \
  }
#endif

#endif

// Функция подсчитывает количество цифр в безнаковом числе числе
int digits_count(int number) {
  int result = 0;   // счетчик
  if (number == 0)  // проверка на ноль
    result = 1;  // возвращаем 1, если число равно нулю
  while (number > 0) {  // цикл, продолжающийся пока число больше нуля
    result++;           // увеличиваем счетчик
    number /= 10;  // делим число на 10 для удаления последней цифры
  }
  return result;  // возвращаем результат
}

char *s21_strerror(int errnum) {
  // Создаем статический массив ошибок
  static char *errarr[] = ERRLIST;
  // Создаем статическую строку для неизвестной ошибки
  static char unknown_error[64] = {0};
  // Инициализируем указатель на ошибку как NULL
  char *error = S21_NULL;

  // Проверяем, находится ли номер ошибки в диапазоне известных ошибок
  if (errnum >= 0 && errnum <= ERRLIST_LEN) {
    // Если номер ошибки в диапазоне, присваиваем его указателю на ошибку
    error = (char *)errarr[errnum];
  } else {
    // Если номер ошибки вне диапазона, формируем сообщение об неизвестной
    // ошибке
#if defined(__APPLE__)
    // Если система Apple, копируем сообщение об ошибке в unknown_error
    s21_strncpy(unknown_error, "Unknown error: ", (s21_size_t)15);
#elif defined(__linux__)
    // Если система Linux, копируем сообщение об ошибке в unknown_error
    s21_strncpy(unknown_error, "Unknown error ", (s21_size_t)14);
#endif
    // Создаем строку для кода ошибки
    char unknown_error_code[10] = {0};
    // Преобразуем номер ошибки в строку
    s21_sprintf(unknown_error_code, "%d", errnum);
    // Добавляем код ошибки в сообщение об ошибке
    s21_strncat(unknown_error, unknown_error_code, digits_count(errnum));
    // Устанавливаем указатель на ошибку на сообщение об ошибке
    error = unknown_error;
  }
  // Возвращаем указатель на ошибку
  return error;
}
/* END geneviej */

// nidamern
char *s21_strcat(char *dest, const char *src) {
  s21_strcpy(dest + s21_strlen(dest), src);
  return dest;
}

// nidamern
char *s21_strchr(const char *str, int c) {
  int i = 0;
  while (str[i] && str[i] != c) {
    ++i;
  }
  return c == str[i] ? (char *)str + i : S21_NULL;
}

// nidamern
int s21_strcmp(const char *str1, const char *str2) {
  for (; *str1 && *str1 == *str2; str1++, str2++) {
  }
  return *str1 - *str2;
}

// nidamern
char *s21_strcpy(char *dest, const char *src) {
  return s21_memcpy(dest, src, s21_strlen(src) + 1);
}

// nidamern
s21_size_t s21_strcspn(const char *str1, const char *str2) {
  s21_size_t i = 0;
  int returnCode = -1;
  int x = 0, y = 0;
  while (*(str1 + x) && returnCode == -1) {
    while (*(str2 + y) && *(str1 + x) != *(str2 + y)) {
      y++;
    }
    if (*(str1 + x) == *(str2 + y)) {
      returnCode = i;
    } else {
      i++;
      x++;
      y = 0;
    }
  }
  if (*(str1 + x) == '\0') {
    returnCode = s21_strlen(str1);
  }
  return returnCode == -1 ? (s21_size_t)S21_NULL : (s21_size_t)returnCode;
}

// nidamern
char *s21_strncat(char *dest, const char *src, s21_size_t n) {
  char *ptr = dest + s21_strlen(dest);

  while (*src != '\0' && n != 0) {
    *ptr++ = *src++;
    n--;
  }
  *ptr = '\0';

  return dest;
}

// nidamern
int s21_strncmp(const char *str1, const char *str2, s21_size_t n) {
  while (n != 0) {
    if (*((char *)str1) != *((char *)str2)) {
      return *((char *)str1) - *((char *)str2);
    }
    n--;
    str1++;
    str2++;
  }

  return 0;
}

// nidamern
char *s21_strncpy(char *dest, const char *src, s21_size_t n) {
  s21_size_t i;
  for (i = 0; i < n && src[i] != '\0'; i++) dest[i] = src[i];
  if (n >= s21_strlen(src)) {
    dest[i] = '\0';
  }
  return dest;
}

// nidamern
s21_size_t s21_strspn(const char *str1, const char *str2) {
  s21_size_t flag = 0;
  s21_size_t count = 0;
  for (s21_size_t i = 0; str1[i]; i++) {
    flag = count;
    for (s21_size_t j = 0; str2[j]; j++) {
      if (str1[i] == str2[j]) {
        count++;
        break;
      }
    }
    if (count == flag) break;
  }
  return count;
}

// Функция для вычисления длины строки geneviej
s21_size_t s21_strlen(const char *str) {
  s21_size_t len = 0;
  if (str) {
    while (str[len]) len++;
  }
  return len;
}

// Функция для поиска первого вхождения любого символа из второго аргумента в
// первый аргумент. geneviej
char *s21_strpbrk(const char *str1, const char *str2) {
  s21_size_t i;  // Индекс для прохода по первой строке.
  s21_size_t k;  // Индекс для прохода по второй строке.
  s21_size_t str1_len = s21_strlen(str1);  // Длина первой строки.
  s21_size_t str2_len = s21_strlen(str2);  // Длина второй строки.
  char *res = S21_NULL;  // Результат поиска, инициализирован нулем.
  // Внешний цикл для прохода по первой строке.
  for (i = 0; i < str1_len; i++) {
    // Внутренний цикл для прохода по второй строке.
    for (k = 0; k < str2_len; k++) {
      // Если текущий символ из первой строки совпадает с символом из второй
      // строки, сохраняем адрес этого символа и возвращаем результат.
      if (str1[i] == str2[k]) {
        res = (char *)&str1[i];
        return res;
      }
    }
  }
  return res;  // Если совпадение не найдено, возвращаем NULL.
}

// Функция для поиска последнего вхождения заданного символа в строку. geneviej
char *s21_strrchr(const char *str, int c) {
  int len = (int)s21_strlen(str);  // Длина строки.
  char *res = S21_NULL;  // Результат поиска, инициализирован нулем.
  for (int i = 0; i <= len; i++) {
    // Если текущий символ совпадает с заданным символом,
    // сохраняем адрес этого символа
    if (str[i] == c) res = (char *)&str[i];
  }
  return res;  // Возвращаем результат поиска.
}

// geneviej
char *s21_strstr(const char *haystack, const char *needle) {
  // Если подстрока пустая, возвращаем начало исходной строки
  if (!*needle) {
    return (char *)haystack;
  }
  // Перебираем исходную строку
  for (; *haystack; haystack++) {
    // Инициализируем указатели на текущие символы в исходной и подстроке
    const char *h = haystack;
    const char *n = needle;
    // Сравниваем символы из исходной строки и подстроки
    while (*h && *n && *h == *n) {
      h++;
      n++;
    }
    // Если все символы подстроки совпали, возвращаем начало найденной подстроки
    // в исходной строке
    if (!*n) {
      return (char *)haystack;
    }
  }
  // Если подстрока не найдена, возвращаем NULL
  return S21_NULL;
}

// geneviej
char *s21_strtok(char *str, const char *delim) {
  // Создаем статическую переменную для хранения последнего найденного токена
  static char *last_token = S21_NULL;
  // Если входная строка равна NULL, то используем последний найденный токен
  if (str == S21_NULL) {
    str = last_token;
  }
  // Если строка или разделители равны NULL, или строка пустая, то возвращаем
  // NULL
  if (str == S21_NULL || *str == '\0' || delim == S21_NULL) {
    return S21_NULL;
  }
  // Пропускаем все символы в начале строки, которые являются разделителями
  while (*str && s21_strchr(delim, *str) != S21_NULL) {
    str++;
  }
  // Если достигли конца строки, то обнуляем последний токен и возвращаем NULL
  if (*str == '\0') {
    last_token = S21_NULL;
    return S21_NULL;
  }
  // Запоминаем начало токена
  char *start_token = str;
  // Перемещаем указатель на следующий разделитель
  str += s21_strcspn(str, delim);
  // Если не достигли конца строки, то заменяем разделитель на NULL и обновляем
  // последний токен
  if (*str != '\0') {
    *str = '\0';
    last_token = str + 1;
  } else {
    // Если достигли конца строки, то обнуляем последний токен
    last_token = S21_NULL;
  }
  // Возвращаем найденный токен
  return start_token;
}
