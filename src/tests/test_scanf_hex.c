#include "test_me.h"

START_TEST(scanf_1_hex) {
  char buffer[] = "1238";
  int value;
  int value2;
  int result = sscanf(buffer, "%x", &value);
  int result2 = s21_sscanf(buffer, "%x", &value2);
  ck_assert_int_eq(result, result2);
  ck_assert_int_eq(value, value2);
}
END_TEST

START_TEST(scanf_2_hex) {
  char buffer[] = "-123";
  int value;
  int value2;
  int result = sscanf(buffer, "%x", &value);
  int result2 = s21_sscanf(buffer, "%x", &value2);
  ck_assert_int_eq(result, result2);
  ck_assert_int_eq(value, value2);
}
END_TEST

START_TEST(scanf_3_hex) {
  char buffer[] = "0x123b";
  int value;
  int value2;
  int result = sscanf(buffer, "%x", &value);
  int result2 = s21_sscanf(buffer, "%x", &value2);
  ck_assert_int_eq(result, result2);
  ck_assert_int_eq(value, value2);
}
END_TEST

START_TEST(scanf_4_hex) {
  char buffer[] = "0X123FB";
  unsigned int value;
  unsigned int value2;
  int result = sscanf(buffer, "%X", &value);
  int result2 = s21_sscanf(buffer, "%X", &value2);
  ck_assert_int_eq(result, result2);
  ck_assert_int_eq(value, value2);
}
END_TEST

Suite *test_scanf_hex(void) {
  Suite *s = suite_create("\033[45m-=S21_SSCANF_HEX=-\033[0m");
  TCase *tc = tcase_create("sscanf_tc");
  suite_add_tcase(s, tc);

  tcase_add_test(tc, scanf_1_hex);
  tcase_add_test(tc, scanf_2_hex);
  tcase_add_test(tc, scanf_3_hex);
  tcase_add_test(tc, scanf_4_hex);

  suite_add_tcase(s, tc);
  return s;
}