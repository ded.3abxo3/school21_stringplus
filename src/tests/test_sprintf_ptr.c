#include "test_me.h"

// One parameter signed
START_TEST(sprintf_1_ptr) {
  char str1[100];
  char str2[100];
  char *str3 = "Test %p Test";
  int val = 0xabcdef;
  ck_assert_int_eq(sprintf(str1, str3, val), s21_sprintf(str2, str3, val));
  ck_assert_pstr_eq(str1, str2);
}
END_TEST

// Three signed parameters
START_TEST(sprintf_2_ptr) {
  char str1[100];
  char str2[100];
  char *str3 = "%p Test %p Test %p";
  int val = 12;
  int val2 = 1717;
  int val3 = 07464;
  ck_assert_int_eq(sprintf(str1, str3, val, val2, val3),
                   s21_sprintf(str2, str3, val, val2, val3));
  ck_assert_pstr_eq(str1, str2);
}
END_TEST

// Three decimal parameters
START_TEST(sprintf_3_ptr) {
  char str1[100];
  char str2[100];
  char *str3 = "%p Test %p Test %p";
  int val = 3015;
  int val2 = 712;
  int val3 = -1;
  ck_assert_int_eq(sprintf(str1, str3, val, val2, val3),
                   s21_sprintf(str2, str3, val, val2, val3));
  ck_assert_pstr_eq(str1, str2);
}
END_TEST

Suite *test_sprintf_ptr(void) {
  Suite *s = suite_create("\033[45m-=S21_SPRINTF_POINTER=-\033[0m");
  TCase *tc = tcase_create("sprintf_tc");

  tcase_add_test(tc, sprintf_1_ptr);
  tcase_add_test(tc, sprintf_2_ptr);
  tcase_add_test(tc, sprintf_3_ptr);

  suite_add_tcase(s, tc);
  return s;
}
