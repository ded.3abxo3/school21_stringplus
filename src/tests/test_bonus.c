#include "test_me.h"

START_TEST(bonus_1) {
  char value1[] = "HELLO";
  char *s2 = s21_to_lower(value1);
  ck_assert_str_eq(s2, "hello");
  if (s2 != NULL) free(s2);
}
END_TEST

START_TEST(bonus_2) {
  char value1[] = "hELLO";
  char *s2 = s21_to_lower(value1);
  ck_assert_str_eq(s2, "hello");
  if (s2 != NULL) free(s2);
}
END_TEST

START_TEST(bonus_3) {
  char value1[] = "HeLLO";
  char *s2 = s21_to_lower(value1);
  ck_assert_str_eq(s2, "hello");
  if (s2 != NULL) free(s2);
}
END_TEST

START_TEST(bonus_4) {
  char value1[] = "HelL.O";
  char *s2 = s21_to_lower(value1);
  ck_assert_str_eq(s2, "hell.o");
  if (s2 != NULL) free(s2);
}
END_TEST

START_TEST(bonus_5) {
  char value1[] = "Hel/LO";
  char *s2 = s21_to_lower(value1);
  ck_assert_str_eq(s2, "hel/lo");
  if (s2 != NULL) free(s2);
}
END_TEST

START_TEST(bonus_6) {
  char value1[] = "hello";
  char *s2 = s21_to_upper(value1);
  ck_assert_str_eq(s2, "HELLO");
  if (s2 != NULL) free(s2);
}
END_TEST

START_TEST(bonus_7) {
  char value1[] = "hellO";
  char *s2 = s21_to_upper(value1);
  ck_assert_str_eq(s2, "HELLO");
  if (s2 != NULL) free(s2);
}
END_TEST

START_TEST(bonus_8) {
  char value1[] = "helLo";
  char *s2 = s21_to_upper(value1);
  ck_assert_str_eq(s2, "HELLO");
  if (s2 != NULL) free(s2);
}
END_TEST

START_TEST(bonus_9) {
  char value1[] = "he.lLo";
  char *s2 = s21_to_upper(value1);
  ck_assert_str_eq(s2, "HE.LLO");
  if (s2 != NULL) free(s2);
}
END_TEST

START_TEST(bonus_10) {
  char value1[] = "he.lL/o";
  char *s2 = s21_to_upper(value1);
  ck_assert_str_eq(s2, "HE.LL/O");
  if (s2 != NULL) free(s2);
}
END_TEST

START_TEST(bonus_11) {
  char src[] = "hello";
  char str[] = "hello";
  int n = 5;
  char *res = s21_insert(src, str, n);
  ck_assert_str_eq(res, "hellohello");
  if (res != NULL) free(res);
}
END_TEST

START_TEST(bonus_12) {
  char src[] = "hello";
  char str[] = "hello";
  int n = 4;
  char *res = s21_insert(src, str, n);
  ck_assert_str_eq(res, "hellhelloo");
  if (res != NULL) free(res);
}
END_TEST

START_TEST(bonus_13) {
  char src[] = "hello";
  char str[] = "hello";
  int n = 3;
  char *res = s21_insert(src, str, n);
  ck_assert_str_eq(res, "helhellolo");
  if (res != NULL) free(res);
}
END_TEST

START_TEST(bonus_14) {
  char src[] = "hello";
  char trim[] = "loeh";
  char *res = s21_trim(src, trim);
  ck_assert_str_eq(res, "");
  free(res);
}
END_TEST

START_TEST(bonus_15) {
  char src[] = "123456789";
  char trim[] = "13689";
  char *res = s21_trim(src, trim);
  ck_assert_str_eq(res, "234567");
  free(res);
}
END_TEST

START_TEST(bonus_16) {
  char src[] = "";
  char trim[] = "13689";
  char *res = s21_trim(src, trim);
  ck_assert_str_eq(res, "");
  free(res);
}
END_TEST

START_TEST(bonus_17) {
  char src[] = "helloworld";
  char trim[] = "HELLOWORLD";
  char *res = s21_trim(src, trim);
  ck_assert_str_eq(res, "helloworld");
  free(res);
}
END_TEST

START_TEST(bonus_18) {
  char src[] = "qwerty12345";
  char trim[] = "y1t2r3e4w5q";
  char *res = s21_trim(src, trim);
  ck_assert_str_eq(res, "");
  free(res);
}
END_TEST

START_TEST(bonus_19) {
  char src[] = "helloworld";
  char trim[] = "";
  char *res = s21_trim(src, trim);
  ck_assert_str_eq(res, "helloworld");
  free(res);
}
END_TEST

Suite *test_bonus(void) {
  Suite *s = suite_create("\033[45m-=S21_bonus=-\033[0m");
  TCase *tc = tcase_create("bonus_tc");

  suite_add_tcase(s, tc);
  tcase_add_test(tc, bonus_1);
  tcase_add_test(tc, bonus_2);
  tcase_add_test(tc, bonus_3);
  tcase_add_test(tc, bonus_4);
  tcase_add_test(tc, bonus_5);
  tcase_add_test(tc, bonus_6);
  tcase_add_test(tc, bonus_7);
  tcase_add_test(tc, bonus_8);
  tcase_add_test(tc, bonus_9);
  tcase_add_test(tc, bonus_10);
  tcase_add_test(tc, bonus_11);
  tcase_add_test(tc, bonus_12);
  tcase_add_test(tc, bonus_13);
  tcase_add_test(tc, bonus_14);
  tcase_add_test(tc, bonus_15);
  tcase_add_test(tc, bonus_16);
  tcase_add_test(tc, bonus_17);
  tcase_add_test(tc, bonus_18);
  tcase_add_test(tc, bonus_19);

  suite_add_tcase(s, tc);
  return s;
}
