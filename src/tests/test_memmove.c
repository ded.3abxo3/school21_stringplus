#include "test_me.h"

START_TEST(memmove_1) {
#line 706
  char str1[9] = "abc";
  char str2[] = "def";
  char str3[9] = "abc";
  s21_size_t n = 1;
  ck_assert_str_eq(memmove(str1, str2, n), s21_memmove(str3, str2, n));
}
END_TEST

START_TEST(memmove_2) {
#line 713
  char str1[9] = "ab\nc";
  char str2[] = "def";
  char str3[9] = "ab\nc";
  int n = 1;
  ck_assert_str_eq(memmove(str1, str2, n), s21_memmove(str3, str2, n));
}
END_TEST

START_TEST(memmove_3) {
#line 720
  char str1[9] = "ab\0c";
  char str2[] = "def";
  char str3[9] = "ab\0c";
  int n = 1;
  ck_assert_str_eq(memmove(str1, str2, n), s21_memmove(str3, str2, n));
}
END_TEST

Suite *test_memmove(void) {
  Suite *s = suite_create("\033[45m-=S21_MEMMOVE=-\033[0m");
  TCase *tc = tcase_create("memchr_tc");

  suite_add_tcase(s, tc);
  tcase_add_test(tc, memmove_1);
  tcase_add_test(tc, memmove_2);
  tcase_add_test(tc, memmove_3);

  suite_add_tcase(s, tc);
  return s;
}
