#ifndef SRC_TESTS_ME_H
#define SRC_TESTS_ME_H

#include <check.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <wchar.h>

#include "../s21_string.h"

/* MEM PART*/
Suite *test_memchr(void);
Suite *test_memcmp(void);
Suite *test_memcpy(void);
Suite *test_memset(void);
Suite *test_memmove(void);
/* STR PART */
Suite *test_strncat(void);
Suite *test_strchr(void);
Suite *test_strncmp(void);
Suite *test_strncpy(void);
Suite *test_strcspn(void);
Suite *test_strerror(void);
Suite *test_strlen(void);
Suite *test_strpbrk(void);
Suite *test_strrchr(void);
Suite *test_strspn(void);
Suite *test_strstr(void);
Suite *test_strtok(void);
/* SPRINTF PART */
Suite *test_sprintf_ptr(void);
Suite *test_sprintf_c(void);
Suite *test_sprintf_e(void);
Suite *test_sprintf_f(void);
Suite *test_sprintf_signed(void);
Suite *test_sprintf_octal(void);
Suite *test_sprintf_unsigned(void);
Suite *test_sprintf_hex(void);
Suite *test_sprintf_HEX(void);
Suite *test_sprintf_percent(void);
Suite *test_sprintf_string(void);
/* SCANF PART */
Suite *test_scanf(void);
Suite *test_scanf_octal(void);
Suite *test_scanf_unsigned(void);
Suite *test_scanf_hex(void);
Suite *test_scanf_ptr(void);
Suite *test_scanf_str(void);
Suite *test_scanf_feg(void);
Suite *test_scanf_exp(void);
Suite *test_scanf_char(void);
/* BONUS PART */
Suite *test_bonus(void);

#endif  // SRC_TESTS_ME_H
