#include "test_me.h"

START_TEST(scanf_1_char) {
  char buffer[] = "Z dfg dflgiu 123";
  char value;
  char value2;
  int result = sscanf(buffer, "%c", &value);
  int result2 = s21_sscanf(buffer, "%c", &value2);
  ck_assert_int_eq(result, result2);
  ck_assert_int_eq(value, value2);
}
END_TEST

START_TEST(scanf_2_char) {
  char buffer[] = "123";
  unsigned int value;
  unsigned int value2;
  int result = sscanf(buffer, "%u", &value);
  int result2 = s21_sscanf(buffer, "%u", &value2);
  ck_assert_int_eq(result, result2);
  ck_assert_uint_eq(value, value2);
}
END_TEST

START_TEST(scanf_3_char) {
  char buffer[] = "123";
  short unsigned int value;
  short unsigned int value2;
  int result = sscanf(buffer, "%hu", &value);
  int result2 = s21_sscanf(buffer, "%hu", &value2);
  ck_assert_int_eq(result, result2);
  ck_assert_uint_eq(value, value2);
}
END_TEST

Suite *test_scanf_char(void) {
  Suite *s = suite_create("\033[45m-=S21_SSCANF_CHAR=-\033[0m");
  TCase *tc = tcase_create("sscanf_tc");
  suite_add_tcase(s, tc);

  tcase_add_test(tc, scanf_1_char);
  tcase_add_test(tc, scanf_2_char);
  tcase_add_test(tc, scanf_3_char);

  suite_add_tcase(s, tc);
  return s;
}