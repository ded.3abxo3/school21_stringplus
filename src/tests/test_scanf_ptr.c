#include "test_me.h"

START_TEST(scanf_1_ptr) {
  char buffer[] = "0x0";
  void *value;
  void *value2;
  int result = sscanf(buffer, "%p", &value);
  int result2 = s21_sscanf(buffer, "%p", &value2);
  ck_assert_int_eq(result, result2);
  ck_assert_ptr_eq(value, value2);
}
END_TEST

START_TEST(scanf_2_ptr) {
  char buffer[] = "0XABCDEF";
  void *value;
  void *value2;
  int result = sscanf(buffer, "%p", &value);
  int result2 = s21_sscanf(buffer, "%p", &value2);
  ck_assert_int_eq(result, result2);
  ck_assert_ptr_eq(value, value2);
}
END_TEST

START_TEST(scanf_3_ptr) {
  char buffer[] = "0xabcdef";
  void *value;
  void *value2;
  int result = sscanf(buffer, "%p", &value);
  int result2 = s21_sscanf(buffer, "%p", &value2);
  ck_assert_int_eq(result, result2);
  ck_assert_ptr_eq(value, value2);
}
END_TEST

Suite *test_scanf_ptr(void) {
  Suite *s = suite_create("\033[45m-=S21_SSCANF_POINTER=-\033[0m");
  TCase *tc = tcase_create("sscanf_tc");
  suite_add_tcase(s, tc);

  tcase_add_test(tc, scanf_1_ptr);
  tcase_add_test(tc, scanf_2_ptr);
  tcase_add_test(tc, scanf_3_ptr);

  suite_add_tcase(s, tc);
  return s;
}