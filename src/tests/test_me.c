#include "test_me.h"

int main(void) {
  int failed = 0;
  Suite *s21_string_test[] = {
      /* MEM TESTS */
      test_memchr(), test_memcmp(), test_memcpy(), test_memset(),
      test_memmove(),
      /* STR TESTS */
      test_strncat(), test_strchr(), test_strncmp(), test_strncpy(),
      test_strcspn(), test_strerror(), test_strlen(), test_strpbrk(),
      test_strrchr(), test_strspn(), test_strstr(), test_strtok(),
      /* SPRINTF TESTS */
      test_sprintf_ptr(), test_sprintf_c(), test_sprintf_f(),
      test_sprintf_octal(), test_sprintf_percent(), test_sprintf_string(),
      test_sprintf_unsigned(), test_sprintf_HEX(), test_sprintf_hex(),
      test_sprintf_signed(),
      /* SCANF TESTS */
      test_scanf(), test_scanf_octal(), test_scanf_unsigned(), test_scanf_hex(),
      test_scanf_ptr(), test_scanf_str(), test_scanf_feg(), test_scanf_exp(),
      test_scanf_char(),
      /* BONUS TESTS */
      test_bonus(),

      NULL};

  for (int i = 0; s21_string_test[i] != NULL; i++) {
    SRunner *sr = srunner_create(s21_string_test[i]);

    srunner_set_fork_status(sr, CK_NOFORK);
    srunner_run_all(sr, CK_NORMAL);

    failed += srunner_ntests_failed(sr);
    srunner_free(sr);
  }
  printf("========= FAILED: %d =========\n", failed);

  return failed == 0 ? 0 : 1;
}
