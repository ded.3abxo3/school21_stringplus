#include "test_me.h"

START_TEST(scanf_1_str) {
  char buffer[] = "1238skdjfhl%%23985.63452all_eyes_on_me(2Pac)";
  char value[200];
  char value2[200];
  int result = sscanf(buffer, "%s", value);
  int result2 = s21_sscanf(buffer, "%s", value2);
  ck_assert_int_eq(result, result2);
  ck_assert_str_eq(value, value2);
}
END_TEST

START_TEST(scanf_2_str) {
  char buffer[] = "1238skdjfhl%%239 85.63452all_eyes_on_me(2Pac)";
  char value[200];
  char value2[200];
  int result = sscanf(buffer, "%s", value);
  int result2 = s21_sscanf(buffer, "%s", value2);
  ck_assert_int_eq(result, result2);
  ck_assert_str_eq(value, value2);
}
END_TEST

START_TEST(scanf_4_str) {
  char buffer[] = "1238skdjfhl%%23985.63452all_eyes_on_me(2Pac)";
  wchar_t value[200];
  wchar_t value2[200];
  int result = sscanf(buffer, "%ls", value);
  int result2 = s21_sscanf(buffer, "%ls", value2);
  ck_assert_int_eq(result, result2);
  ck_assert_str_eq((char *)value, (char *)value2);
}
END_TEST

Suite *test_scanf_str(void) {
  Suite *s = suite_create("\033[45m-=S21_SSCANF_STRING=-\033[0m");
  TCase *tc = tcase_create("sscanf_tc");
  suite_add_tcase(s, tc);

  tcase_add_test(tc, scanf_1_str);
  tcase_add_test(tc, scanf_2_str);
  tcase_add_test(tc, scanf_4_str);

  suite_add_tcase(s, tc);
  return s;
}