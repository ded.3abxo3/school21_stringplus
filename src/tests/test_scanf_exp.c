#include "test_me.h"

START_TEST(scanf_1_exp) {
  char buffer[] = "-1.151e-02";
  float value;
  float value2;
  int result = sscanf(buffer, "%e", &value);
  int result2 = s21_sscanf(buffer, "%e", &value2);
  ck_assert_int_eq(result, result2);
  ck_assert_float_eq(value, value2);
}
END_TEST

START_TEST(scanf_2_exp) {
  char buffer[] = "1";
  float value;
  float value2;
  int result = sscanf(buffer, "%e", &value);
  int result2 = s21_sscanf(buffer, "%e", &value2);
  ck_assert_int_eq(result, result2);
  ck_assert_float_eq(value, value2);
}
END_TEST
/*
START_TEST(scanf_2_feg) {
  char buffer[] = "1238.151682";
    float value;
    float value2;
    int result = sscanf(buffer, "%g", &value);
    int result2 = s21_sscanf(buffer, "%g", &value2);
    ck_assert_int_eq(result, result2);
    ck_assert_float_eq(value, value2);
}
END_TEST
*/
/*
START_TEST(scanf_3_feg) {
  char buffer[] = "1238.151682";
    double value;
    double value2;
    int result = sscanf(buffer, "%le", &value);
    int result2 = sscanf(buffer, "%le", &value2);
    ck_assert_int_eq(result, result2);
    ck_assert_double_eq(value, value2);
}
END_TEST
*/

Suite *test_scanf_exp(void) {
  Suite *s = suite_create("\033[45m-=S21_SSCANF_EXP=-\033[0m");
  TCase *tc = tcase_create("sscanf_tc");
  suite_add_tcase(s, tc);

  tcase_add_test(tc, scanf_1_exp);
  tcase_add_test(tc, scanf_2_exp);
  // tcase_add_test(tc, scanf_3_feg);
  // tcase_add_test(tc, scanf_4_feg);

  suite_add_tcase(s, tc);
  return s;
}