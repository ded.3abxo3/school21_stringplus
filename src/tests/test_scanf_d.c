#include "test_me.h"

START_TEST(scanf_1) {
  char buffer[] = "123";
  int value;
  int value2;
  int result = sscanf(buffer, "%d", &value);
  int result2 = s21_sscanf(buffer, "%d", &value2);
  ck_assert_int_eq(result, result2);
  ck_assert_int_eq(value, value2);
}
END_TEST

START_TEST(scanf_2) {
  char buffer[] = "-123";
  int value;
  int value2;
  int result = sscanf(buffer, "%d", &value);
  int result2 = s21_sscanf(buffer, "%d", &value2);
  ck_assert_int_eq(result, result2);
  ck_assert_int_eq(value, value2);
}
END_TEST

START_TEST(scanf_3) {
  char buffer[] = "12385897461245";
  long int value;
  long int value2;
  int result = sscanf(buffer, "%11ld", &value);
  int result2 = s21_sscanf(buffer, "%11ld", &value2);
  ck_assert_int_eq(result, result2);
  ck_assert_int_eq(value, value2);
}
END_TEST
/*
START_TEST(scanf_4) {
  char buffer[] = "123.456";
    int value;
    int value2;
    int result = sscanf(buffer, "%d", &value);
    int result2 = s21_sscanf(buffer, "%d", &value2);
    ck_assert_int_eq(result, result2);
    ck_assert_int_eq(value, value2);
}
END_TEST
*/
/*
START_TEST(scanf_5) {
  char buffer[] = "-123";
    int value;
    int value2;
    int result = sscanf(buffer, "%X", &value);
    int result2 = s21_sscanf(buffer, "%X", &value2);
    ck_assert_int_eq(result, result2);
    ck_assert_int_eq(value, value2);
}
END_TEST
*/
/*
START_TEST(scanf_6) {
  char buffer[] = "123";
    int value;
    int value2;
    int result = sscanf(buffer, "%o", &value);
    int result2 = s21_sscanf(buffer, "%o", &value2);
    ck_assert_int_eq(result, result2);
    ck_assert_int_eq(value, value2);
}
END_TEST*/
/*
START_TEST(scanf_7) {
  char buffer[] = "-abcd";
    int value;
    int value2;
    int result = sscanf(buffer, "%X", &value);
    int result2 = s21_sscanf(buffer, "%X", &value2);
    ck_assert_int_eq(result, result2);
    ck_assert_int_eq(value, value2);
}
END_TEST*/

Suite *test_scanf(void) {
  Suite *s = suite_create("\033[45m-=S21_SSCANF=-\033[0m");
  TCase *tc = tcase_create("sscanf_tc");
  suite_add_tcase(s, tc);

  tcase_add_test(tc, scanf_1);
  tcase_add_test(tc, scanf_2);
  tcase_add_test(tc, scanf_3);
  // tcase_add_test(tc, scanf_4);
  // tcase_add_test(tc, scanf_5);
  // tcase_add_test(tc, scanf_6);
  // tcase_add_test(tc, scanf_7);

  suite_add_tcase(s, tc);
  return s;
}