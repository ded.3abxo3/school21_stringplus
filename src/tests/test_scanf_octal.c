#include "test_me.h"

START_TEST(scanf_1_oct) {
  char buffer[] = "1238";
  int value;
  int value2;
  int result = sscanf(buffer, "%o", &value);
  int result2 = s21_sscanf(buffer, "%o", &value2);
  ck_assert_int_eq(result, result2);
  ck_assert_int_eq(value, value2);
}
END_TEST

START_TEST(scanf_2_oct) {
  char buffer[] = "-123";
  int value;
  int value2;
  int result = sscanf(buffer, "%o", &value);
  int result2 = s21_sscanf(buffer, "%o", &value2);
  ck_assert_int_eq(result, result2);
  ck_assert_int_eq(value, value2);
}
END_TEST

START_TEST(scanf_3_oct) {
  char buffer[] = "01237";
  int value;
  int value2;
  int result = sscanf(buffer, "%o", &value);
  int result2 = s21_sscanf(buffer, "%o", &value2);
  ck_assert_int_eq(result, result2);
  ck_assert_int_eq(value, value2);
}
END_TEST

START_TEST(scanf_4_oct) {
  char buffer[] = "123.456";
  int value;
  int value2;
  int result = sscanf(buffer, "%o", &value);
  int result2 = s21_sscanf(buffer, "%o", &value2);
  ck_assert_int_eq(result, result2);
  ck_assert_int_eq(value, value2);
}
END_TEST

Suite *test_scanf_octal(void) {
  Suite *s = suite_create("\033[45m-=S21_SSCANF_OCTAL=-\033[0m");
  TCase *tc = tcase_create("sscanf_tc");
  suite_add_tcase(s, tc);

  tcase_add_test(tc, scanf_1_oct);
  tcase_add_test(tc, scanf_2_oct);
  tcase_add_test(tc, scanf_3_oct);
  tcase_add_test(tc, scanf_4_oct);

  suite_add_tcase(s, tc);
  return s;
}