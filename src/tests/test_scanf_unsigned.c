#include "test_me.h"

START_TEST(scanf_1_unsigned) {
  char buffer[] = "1238";
  unsigned int value;
  unsigned int value2;
  int result = sscanf(buffer, "%i", &value);
  int result2 = s21_sscanf(buffer, "%i", &value2);
  ck_assert_uint_eq(result, result2);
  ck_assert_uint_eq(value, value2);
}
END_TEST

START_TEST(scanf_2_unsigned) {
  char buffer[] = "0123";
  unsigned int value;
  unsigned int value2;
  int result = sscanf(buffer, "%i", &value);
  int result2 = s21_sscanf(buffer, "%i", &value2);
  ck_assert_uint_eq(result, result2);
  ck_assert_uint_eq(value, value2);
}
END_TEST

START_TEST(scanf_3_unsigned) {
  char buffer[] = "0xabcdef";
  unsigned int value;
  unsigned int value2;
  int result = sscanf(buffer, "%i", &value);
  int result2 = s21_sscanf(buffer, "%i", &value2);
  ck_assert_uint_eq(result, result2);
  ck_assert_uint_eq(value, value2);
}
END_TEST

START_TEST(scanf_4_unsigned) {
  char buffer[] = "123";
  long unsigned int value;
  long unsigned int value2;
  int result = sscanf(buffer, "%lu", &value);
  int result2 = s21_sscanf(buffer, "%lu", &value2);
  ck_assert_int_eq(result, result2);
  ck_assert_uint_eq(value, value2);
}
END_TEST

START_TEST(scanf_5_unsigned) {
  char buffer[] = "-1";
  unsigned int value;
  unsigned int value2;
  int result = sscanf(buffer, "%i", &value);
  int result2 = s21_sscanf(buffer, "%i", &value2);
  ck_assert_uint_eq(result, result2);
  ck_assert_uint_eq(value, value2);
}
END_TEST

Suite *test_scanf_unsigned(void) {
  Suite *s = suite_create("\033[45m-=S21_SSCANF_UNSIGNED=-\033[0m");
  TCase *tc = tcase_create("sscanf_tc");
  suite_add_tcase(s, tc);

  tcase_add_test(tc, scanf_1_unsigned);
  tcase_add_test(tc, scanf_2_unsigned);
  tcase_add_test(tc, scanf_3_unsigned);
  tcase_add_test(tc, scanf_4_unsigned);
  tcase_add_test(tc, scanf_5_unsigned);

  suite_add_tcase(s, tc);
  return s;
}